<?php
echo "<h1>Function Tukar Besar Kecil</h1>";
function tukar_besar_kecil($string)
{
    //kode di sini
    for ($i = 0; $i < strlen($string); $i++) {
        $output[$i] = ord($string[$i]);
        if ($output[$i] >= 65 && $output[$i] <= 90) {
            echo chr($output[$i] + 32);
        } else if ($output[$i] >= 97 && $output[$i] <= 122) {
            echo chr($output[$i] - 32);
        } else if ($output[$i] >= 32 && $output[$i] <= 63) {
            echo chr($output[$i]);
        }
    }
}

// TEST CASES
echo tukar_besar_kecil('Hello World') . '<br>'; // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY') . '<br>'; // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!') . '<br>'; // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me') . '<br>'; // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW') . '<br>'; // "001-a-3-5tRDyw"
